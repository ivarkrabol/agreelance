const categoryIdSelect = document.getElementById('id_category_id');
const categorySuggestionInput = document.getElementById('id_category_suggestion');
const categorySuggestionGroup = document.getElementById('group_category_suggestion');
const taskContainer = document.getElementById('task_container');
const firstTask = document.getElementsByClassName('task')[0];
const firstTaskCurrencySelect = document.getElementById('id_budget_currency');
const taskTemplate = firstTask.innerHTML;
const addTaskButton = document.getElementById('add_task');
const removeTaskButton = document.getElementById('remove_task');

categoryIdSelect.addEventListener('input', updateSuggestionVisibility);
window.addEventListener('popstate', updateSuggestionVisibility);
updateSuggestionVisibility();
setTimeout(updateSuggestionVisibility, 100); // Fixes bug with back button in chrome

initTask(firstTask);
firstTaskCurrencySelect.disabled = false;

firstTaskCurrencySelect.addEventListener('change', updateCurrency);
addTaskButton.addEventListener('click', addTask);
removeTaskButton.addEventListener('click', removeTask);

function updateSuggestionVisibility() {
    if (categoryIdSelect.value === '__NEW__') {
        categorySuggestionGroup.classList.remove('hidden');
        categorySuggestionInput.disabled = false;
    } else {
        categorySuggestionGroup.classList.add('hidden');
        categorySuggestionInput.disabled = true;
    }
}

function initTask(task) {
    const budgetInput = task.getElementsByClassName('budget-input')[0];
    budgetInput.addEventListener('input', () => {
        if (budgetInput.value.indexOf('.') >= 0
            && budgetInput.value.length > budgetInput.value.indexOf('.') + 3
        ) {
            budgetInput.value = budgetInput.value.substring(0, budgetInput.value.indexOf('.') + 3);
        }
    });
}

function addTask() {
    const newTask = document.createElement('div');
    newTask.className = firstTask.className;
    newTask.innerHTML = taskTemplate;
    taskContainer.appendChild(newTask);
    initTask(newTask);
    const currencySelect = newTask.getElementsByClassName('budget-currency')[0];
    currencySelect.value = firstTaskCurrencySelect.value;
    currencySelect.name = '';
    currencySelect.id = '';
}

function removeTask() {
    const taskChildren = taskContainer.childElementCount;
    if (taskChildren === 1) {
        alert('A project needs a minimum of 1 task');
    } else {
        taskContainer.removeChild(taskContainer.lastChild);
    }
}

function updateCurrency() {
    Array.from(document.getElementsByClassName('budget-currency')).forEach(select => {
        select.value = firstTaskCurrencySelect.value;
    });

}
