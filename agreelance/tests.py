from unittest import expectedFailure

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from projects.models import Project, Task, ProjectCategory
from user.models import Profile


def create_profile(username):
    user = User.objects.create(username=username)
    return Profile.objects.get(user=user)


def create_project_category(name):
    return ProjectCategory.objects.create(
        name=name,
        approved=True,
    )


def create_project(project_owner):
    return Project.objects.create(
        user=project_owner,
        title='Some Project',
        description='Some description.',
        category_id=1,
    )


def create_task(project):
    return Task.objects.create(
        project=project,
        title='Some Task',
        description='Some description.',
        budget=100,
    )


def boundary_value_args(values, value_type='value', _=(lambda value: value)):
    # 0: min-, 1: min, 2: min+, 3: nom, 4: max-, 5: max, 6: max+

    messages = [
        'The {7} {0} is below the minimum ({1}) and should not be accepted',
        'The {7} {1} is the minimum and should be accepted',
        'The {7} {2} is above the minimum ({1}) and should be accepted',
    ]

    if values[5] is not None:
        messages += [
            'The {7} {3} is within the legal range [{1}, {5}] and should be accepted',
            'The {7} {4} is below the maximum ({5}) and should be accepted',
            'The {7} {5} is the maximum and should be accepted',
            'The {7} {6} exceeds the maximum ({5}) and should not be accepted',
        ]

    format_args = (*map(lambda v: None if v is None else _(v), values), value_type)

    for i, value, message in zip(range(7), values, messages):
        if value is not None:
            # Boundary value, expected success, message if failed
            yield value, 1 <= i <= 5, message.format(*format_args)


def int_boundary_values(min_=0, max_=None):
    # 0: min-, 1: min, 2: min+, 3: nom, 4: max-, 5: max, 6: max+
    return [min_ - 1, min_, min_ + 1, *([None] * 4 if max_ is None else [(max_ + min_) // 2, max_ - 1, max_, max_ + 1])]


def string_length_boundary_values(min_=0, max_=None, repeat='A'):
    # 0: min-, 1: min, 2: min+, 3: nom, 4: max-, 5: max, 6: max+
    values = []
    for length in int_boundary_values(min_, max_):
        values.append(None if (length is None or length < 0) else (repeat * (length // len(repeat) + 1))[:length])
    return values


class MakeTaskOfferBoundaryValueTests(TestCase):
    def setUp(self):
        self.profile = create_profile('test')
        self.client.force_login(self.profile.user)
        self.project = create_project(create_profile('owner'))
        self.task = create_task(self.project)

    def make_task_offer(self, title, description, price):
        response = self.client.post(reverse('project_view', kwargs={'project_id': self.project.id}), {
            'offer_submit': True,
            'taskvalue': self.task.id,
            'title': title,
            'description': description,
            'price': price,
        })
        task_offer = self.task.taskoffer_set.first()
        self.assertEqual(response.status_code, 200)
        return task_offer is not None and task_offer.offerer == self.profile

    # Accepts (truncates) inputs that are too long
    @expectedFailure
    def test_title_length(self):
        boundary_values = string_length_boundary_values(1, 200)
        for title, expected_success, message in boundary_value_args(boundary_values, 'title length', len):
            success = self.make_task_offer(title, 'Bar.', 100)
            self.assertEquals(expected_success, success, message)

    # Accepts (truncates) inputs that are too long
    @expectedFailure
    def test_description_length(self):
        boundary_values = string_length_boundary_values(1, 500)
        for description, expected_success, message in boundary_value_args(boundary_values, 'description length', len):
            success = self.make_task_offer('Foo', description, 100)
            self.assertEquals(expected_success, success, message)

    # Accepts negative prices
    @expectedFailure
    def test_price(self):
        boundary_values = int_boundary_values(max_=1000000)
        for price, expected_success, message in boundary_value_args(boundary_values, 'price'):
            success = self.make_task_offer('Foo', 'Bar.', price)
            self.assertEquals(expected_success, success, message)


class SignUpBoundaryValueTests(TestCase):
    def setUp(self):
        self.category = create_project_category('Foo')

    def fill_with_defaults(self, kwargs):
        return {
            'username': 'foobar',
            'first_name': 'Foo',
            'last_name': 'Bar',
            'categories': [self.category.id],
            'company': 'Foobar inc.',
            'email': 'foo@bar.test',
            'password1': 'verysecret',
            'phone_number': '12345678',
            'country': 'Norway',
            'state': 'Trondelag',
            'city': 'Trondheim',
            'postal_code': '0000',
            'street_address': 'Foobar Street 1',
            **kwargs,
        }

    def sign_up(self, **kwargs):
        kwargs = self.fill_with_defaults(kwargs)
        kwargs['email_confirmation'] = kwargs['email']
        kwargs['password2'] = kwargs['password1']

        response = self.client.post(reverse('signup'), kwargs)
        self.assertIn(response.status_code, (200, 302))
        user = User.objects.first()
        success = user is not None and user.profile is not None and user.username == kwargs['username']
        if user is not None:
            user.delete()
        return success

    def _test_simple_field_length(self, key, name, min_, max_, repeat='A'):
        boundary_values = string_length_boundary_values(min_, max_, repeat)
        for value, expected_success, message in boundary_value_args(boundary_values, name, len):
            success = self.sign_up(**{key: value})
            self.assertEquals(expected_success, success, message)

    def test_username_length(self):
        self._test_simple_field_length('username', 'username length', 1, 150)

    def test_first_name_length(self):
        self._test_simple_field_length('first_name', 'first name length', 1, 30)

    def test_last_name_length(self):
        self._test_simple_field_length('last_name', 'last name length', 1, 30)

    def test_company_length(self):
        self._test_simple_field_length('company', 'company length', 0, 30)

    def test_email_length(self):
        suffix = '@f.oo'
        boundary_values = [name + suffix for name in string_length_boundary_values(1, 254 - len(suffix))]
        for email, expected_success, message in boundary_value_args(boundary_values, 'email length', len):
            success = self.sign_up(email=email)
            self.assertEquals(expected_success, success, message)

    def test_password_length(self):
        self._test_simple_field_length('password1', 'password length', 8, None, 'Aa0!')

    def test_phone_number_length(self):
        self._test_simple_field_length('phone_number', 'phone number length', 1, 50)

    def test_country_length(self):
        self._test_simple_field_length('country', 'country length', 1, 50)

    def test_state_length(self):
        self._test_simple_field_length('state', 'state length', 1, 50)

    def test_city_length(self):
        self._test_simple_field_length('city', 'city length', 1, 50)

    def test_postal_code_length(self):
        self._test_simple_field_length('postal_code', 'postal code length', 1, 50)

    def test_street_address_length(self):
        self._test_simple_field_length('street_address', 'street address length', 1, 50)
