from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from user.models import Profile
from .models import Project, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, CURRENCIES


class ProjectForm(forms.ModelForm):
    class CategoryChoiceField(forms.ModelChoiceField):
        def __init__(self, queryset):
            super().__init__(queryset)
            self._selection = None
            self._init_choices()

        def _init_choices(self):
            self._set_choices(list(self._get_choices()) + [('__NEW__', '-- Add new category --')])

        def to_python(self, value):
            if value == '__NEW__':
                return ProjectCategory(name='__NEW__')
            return super().to_python(value)

    title = forms.CharField(max_length=200)
    description = forms.Textarea()
    category_id = CategoryChoiceField(queryset=ProjectCategory.get_approved())
    category_suggestion = forms.CharField(label='New category', max_length=200, min_length=3, required=False)

    category_suggestion_regex = '^(?!(' + '|'.join(ProjectCategory.objects.values_list('name', flat=True)) + ')$).*'
    currencies = CURRENCIES

    def clean_category_suggestion(self):
        suggested_name = self.cleaned_data['category_suggestion']
        if suggested_name:
            existing_category = ProjectCategory.objects.filter(name__iexact=suggested_name).first()
            if existing_category is not None:
                raise ValidationError('Category already exists.')
        return suggested_name

    class Meta:
        model = Project
        fields = ('title', 'description', 'category_id')


class TaskFileForm(forms.ModelForm):
    file = forms.FileField()

    class Meta:
        model = TaskFile
        fields = ('file',)


class ProjectStatusForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('status',)


class TaskOfferForm(forms.ModelForm):
    title = forms.CharField(max_length=200)
    description = forms.Textarea()
    price = forms.NumberInput()

    class Meta:
        model = TaskOffer
        fields = ('title', 'description', 'price',)


class TaskOfferResponseForm(forms.ModelForm):
    feedback = forms.Textarea()

    class Meta:
        model = TaskOffer
        fields = ('status', 'feedback')


class TaskDeliveryResponseForm(forms.ModelForm):
    feedback = forms.Textarea()

    class Meta:
        model = Delivery
        fields = ('status', 'feedback')


PERMISSION_CHOICES = (
    ('Read', 'Read'),
    ('Write', 'Write'),
    ('Modify', 'Modify'),
)


class TaskPermissionForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all())
    permission = forms.ChoiceField(choices=PERMISSION_CHOICES)


class DeliveryForm(forms.ModelForm):
    comment = forms.Textarea()
    file = forms.FileField()

    class Meta:
        model = Delivery
        fields = ('comment', 'file')


class TeamForm(forms.ModelForm):
    name = forms.CharField(max_length=50)

    class Meta:
        model = Team
        fields = ('name',)


class TeamAddForm(forms.ModelForm):
    members = forms.ModelMultipleChoiceField(queryset=Profile.objects.all(), label='Members with read')

    class Meta:
        model = Team
        fields = ('members',)
