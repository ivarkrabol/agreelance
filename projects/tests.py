from django.test import TestCase
from django.urls import reverse

from agreelance.tests import create_profile, create_project, create_task
from projects.models import Project, TaskOffer, Team
from projects.views import get_user_task_permissions


def create_task_offer(task, offerer, status=TaskOffer.PENDING):
    return TaskOffer.objects.create(
        task=task,
        title='Some Task Offer',
        description='Some description.',
        price=100,
        offerer=offerer,
        status=status,
        feedback='',
    )


def create_team(task):
    return Team.objects.create(
        name='Some Team',
        task=task,
        write=True,
    )


class ProjectViewTests(TestCase):
    def setUp(self):
        self.profile = create_profile('test')
        self.client.force_login(self.profile.user)

    def send_request(self, project, data):
        return self.client.post(reverse('project_view', kwargs={'project_id': project.id}), data)

    def test_accept_offer(self):
        project = create_project(self.profile)
        task = create_task(project)
        task_offer = create_task_offer(task, create_profile('offerer'))
        response = self.send_request(project, {
            'offer_response': True,
            'taskofferid': task_offer.id,
            'status': TaskOffer.ACCEPTED,
            'feedback': 'Some feedback.',
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(task.accepted_task_offer(), task_offer)

    def test_change_status(self):
        project = create_project(self.profile)
        response = self.send_request(project, {
            'status_change': True,
            'status': Project.FINISHED,
        })
        project.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(project.status, Project.FINISHED)

    def test_submit_offer(self):
        project = create_project(create_profile('owner'))
        task = create_task(project)
        response = self.send_request(project, {
            'offer_submit': True,
            'taskvalue': task.id,
            'title': 'Some Task Offer',
            'description': 'Some description.',
            'price': 100,
        })
        task_offer = task.taskoffer_set.first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(task_offer.offerer, self.profile)


class GetUserTaskPermissionsTests(TestCase):
    def setUp(self):
        self.profile = create_profile('test')

    def test_project_owner(self):
        task = create_task(create_project(self.profile))
        permissions = get_user_task_permissions(self.profile.user, task)
        self.assertEquals({
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }, permissions)

    def test_accepted_task_offerer(self):
        task = create_task(create_project(create_profile('owner')))
        create_task_offer(task, self.profile, TaskOffer.ACCEPTED)
        permissions = get_user_task_permissions(self.profile.user, task)
        self.assertEquals({
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }, permissions)

    def test_normal(self):
        task = create_task(create_project(create_profile('owner')))
        task.read.add(self.profile)
        task.write.add(self.profile)
        task.modify.add(self.profile)
        team = create_team(task)
        team.members.add(self.profile)
        permissions = get_user_task_permissions(self.profile.user, task)
        self.assertEquals({
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'view_task': True,
            'upload': True,
        }, permissions)
